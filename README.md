# serde_examples

## [Overview of Serde](https://serde.rs/)

- Generic **Ser**ialization and **De**serialization crate
- 8th most downloaded on [crates.io](https://crates.io/crates/serde)
- Many supported [Data Formats](https://serde.rs/#data-formats)
- Easy to use with `#[derive(Serialize, Deserialize)]`

## Why?

- We need a way to represent data when send/store it
- We want ease of coversion to/from these formats
- Useful in creation of & interaction with APIs

## How?

- Most often it will work to [derive](https://serde.rs/derive.html) Serialization and Deserialization
-