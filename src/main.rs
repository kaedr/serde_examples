

fn main() {
    game_demo::run();
}

mod web_api_demo {
    use std::fs;

    use serde::{Serialize, Deserialize};
    use tiny_http::{Server, Response};

    #[derive(Serialize, Deserialize)]
    struct Resource {
        name: String,
        url: String,
    }

    pub fn run() {
        let resources: Vec<Resource> = serde_yaml::from_str(&fs::read_to_string("./resources.yaml").unwrap()).unwrap();
        let server = Server::http("0.0.0.0:8000").unwrap();
        for request in server.incoming_requests() {
            println!("received request! url: {:?}",
                request.url(),
            );
            let mut response = Response::from_string("Resource Not Found").with_status_code(404);
            for resource in &resources {
                if request.url() == resource.name {
                    response = Response::from_string(serde_json::to_string_pretty(resource).unwrap());
                }
            }
            request.respond(response).unwrap();
        }
    }
}

mod game_demo {
    use std::{thread, net::{TcpListener, TcpStream}, io::{Write, Read, self}};
    use rand::{Rng, thread_rng};
    use rmp_serde::Serializer;
    use serde::{Serialize, Deserialize};

    #[derive(Serialize, Deserialize)]
    enum Room {
        Empty,
        PitTrap,
        Monster,
        Exit,
    }

    pub fn run() {
        let listener = TcpListener::bind("127.0.0.1:4321").unwrap();
        thread::spawn(move || {
            for stream in listener.incoming() {
                thread::spawn(|| {
                    let mut stream = stream.unwrap();
                    let mut out_buf = Vec::new();
                    let mut out_serializer = Serializer::new(&mut out_buf);
                    Room::Empty.serialize(&mut out_serializer).unwrap();
                    stream.write_all(&out_buf).unwrap();
                    loop {
                        // Valid commands are only ever one byte
                        let mut buf = [1];
                        let mut out_buf = Vec::new();
                        let mut out_serializer = Serializer::new(&mut out_buf);
                        let left = random_room();
                        let center = random_room();
                        let right =  random_room();
                        match stream.read(&mut buf) {
                            Ok(num_read) => {
                                if num_read == 0 { break; }
                                match buf.last().unwrap() {
                                    b'l' => left.serialize(&mut out_serializer).unwrap(),
                                    b'c' => center.serialize(&mut out_serializer).unwrap(),
                                    b'r' => right.serialize(&mut out_serializer).unwrap(),
                                    _ => Room::Empty.serialize(&mut out_serializer).unwrap(),
                                }
                                stream.write_all(&out_buf).unwrap();
                            },
                            Err(_) => break,
                        }
                    }
                });
            }
        });

        let mut client = TcpStream::connect("127.0.0.1:4321").unwrap();
        loop {
            let mut read_buf = [0u8; 512];
            client.read(&mut read_buf).unwrap();
            let room: Room = rmp_serde::from_slice(&read_buf).unwrap();
            match room {
                Room::Empty => {
                    println!("You stand in an empty room, three paths are before you, choose one:");
                    println!("'l' for left, 'c' for center, 'r' for right");
                },
                Room::PitTrap => {
                    println!("You have fallen into a pit and died...");
                    break;
                },
                Room::Monster => {
                    println!("You have been eaten by a monster and died...");
                    break;
                },
                Room::Exit => {
                    println!("You have reached the exit, you win!");
                    break;
                },
            }
            let user_input = get_input();
            client.write_all(user_input.as_bytes()).unwrap();
        }
    }

    fn get_input() -> String {
        let mut user_input = String::new();
        io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read user input");
        user_input.trim().into()
    }


    fn random_room() -> Room {
        let num = thread_rng().gen_range(0..10);
        match num {
            6 | 7 => Room::PitTrap,
            8 => Room::Monster,
            9 => Room::Exit,
            _ => Room::Empty,
        }
    }
}